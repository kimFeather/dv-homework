// Everything
// implement function every

function every(arraies, test) {
    let counts = []
    for (let array of arraies) {
      let name = test(array)
      let known = counts.findIndex(c => c.name == name)
      if (known == -1) {
        counts.push({name, count: 1})
      } else {
        counts[known].count++
      }
    }
    return counts
  }

console.log(every([1, 3, 5], n => n < 10)) // true
console.log(every([2, 4, 16], n => n < 10)) // false
console.log(every([], n => n < 10)) // true

